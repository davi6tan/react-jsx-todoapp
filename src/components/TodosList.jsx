import React from "react";
import Todo from "./Todo";

const TodosList = props =>{

    return (
        <div>
            {
                props.todos.map(t => (
                        <Todo
                            key={t.id}
                            todo={t}
                            onSetRemoveTodo={props.onSetRemoveTodo}
                            onSaveEditTodo={props.onSaveEditTodo}
                            onSetEditTodo={props.onSetEditTodo}
                            onSetCompletedTodo={props.onSetCompletedTodo}
                        />
                    )
                )
            }

        </div>
    )


}

export default TodosList;