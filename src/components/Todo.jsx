import React, {Component} from "react";
import PropTypes from 'prop-types';

class Todo extends Component {
    constructor(props) {
        super(props);
        this.todo = props.todo
    }

    handleEdit = (e) => {
        e.preventDefault();
        this.props.onSetEditTodo(this.todo.id);

    }

    onSubmit = (e) => {
        e.preventDefault();
        const text = this._todoText.value.trim();
        this.props.onSaveEditTodo(this.todo.id, text);
    }
 


    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <div id="todo-list" >
                    <p >
                        {

                            this.todo.edit
                                ? <input type="text"
                                         defaultValue={this.todo.text}
                                         ref={input => this._todoText = input}
                                />
                                : <label className={(this.todo.completed) ? "completed" : ""} onDoubleClick={this.handleEdit}>{this.todo.text}</label>
                        }
                        <button
                            type="button"
                            className="btn btn-success btn-completed-list-item"
                            onClick={e => this.props.onSetCompletedTodo(this.todo, e)}>

                            <i className="glyphicon  glyphicon-ok" />

                        </button>

                        <button
                            type="button"
                            className="btn btn-danger btn-removable-list-item"
                            onClick={e => this.props.onSetRemoveTodo(this.todo, e)}>

                            <i className="glyphicon  glyphicon-trash" />

                        </button>
                    </p>
                </div>
            </form>
        );
    }
}


Todo.propTypes = {
    onSaveEditTodo: PropTypes.func.isRequired,
    onSetEditTodo: PropTypes.func.isRequired
}
export default Todo;