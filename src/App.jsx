import React, {Component} from 'react';
import {Modal} from 'react-bootstrap';
//import './App.css';
import TodosList from './components/TodosList';
import TodoInput from './components/TodoInput';


class App extends Component {
    constructor(props) {
        super(props);
        this._nextTodoId = 1;
        this.state = {todos: []}
    }

    componentDidMount() {
        this.todoForm.focusInput();
    }

    render() {
        const {todos} = this.state;

        return (
            <div>
                <div className="Main container">
                    <div className="static-modal">
                        <Modal.Dialog>
                            <span className="todo-title">todos</span>

                            <div id="scroll-wrap">
                                <TodoInput
                                    onAddTodo={this.onAddTodo}
                                    ref={form => this.todoForm = form}
                                />

                                <TodosList
                                    todos={todos}
                                    onSetRemoveTodo={this.onSetRemoveTodo}
                                    onSaveEditTodo={this.onSaveEditTodo}
                                    onSetEditTodo={this.onSetEditTodo}
                                    onSetCompletedTodo={this.onSetCompletedTodo}

                                />

                            </div>

                            <Modal.Footer>@2018</Modal.Footer>

                        </Modal.Dialog>
                    </div>

                </div>
            </div>


        );
    }

    // add new
    onAddTodo = (text) => {
        this.setState({
            todos: this.state.todos.concat({
                id: this._nextTodoId++,
                text,
                edit: false,
                completed:false
            })
        })

    }
    // finished

    // edit
    onSaveEditTodo = (id, text) => {
        this.setState({ //save this state
            todos: this.state.todos.map(t => {
                let tmp = t
                if (t.id === id) {
                    tmp.text = text
                    tmp.edit = false
                }
                return tmp;
            })
        })
    }

    onSetEditTodo = (id) => {//true to edit
        this.setState({ //save this state
            todos: this.state.todos.map(t => {
                let tmp = t
                if (t.id === id) {
                    tmp.edit = true
                }
                return tmp;
            })
        })
    }
    /// delete
    onSetRemoveTodo = (todo) => {
        const current_id = todo.id;
        let {todos} = this.state;
        this.setState({todos: todos.filter(t => t.id !== current_id)})
    }

    onSetCompletedTodo = (todo) => {
        const current_id = todo.id;
        this.setState({ //save this state
            /// toggle
            todos: this.state.todos.map(t => {
                let tmp = t
                if (t.id === current_id) {
                    tmp.completed = !tmp.completed
                }
                return tmp;
            })
        })
    }

}

export default App;
